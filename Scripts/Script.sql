create table mangly.t_song(
song_id integer primary key,
song_name varchar(4000),
singer_names varchar(4000),
song_lyricist_names varchar(4000)
);

create sequence seq_song;

select * from seq_song;

alter table mangly.t_song  alter parent_id drop not null;

alter table mangly.t_song add column lyric varchar(4000);

select * from mangly.t_song;

insert into mangly.t_song values(1,'test','test','test');

