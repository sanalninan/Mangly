package com.mangly.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mangly.dao.SongDao;
import com.mangly.entity.Song;

@Service
public class SongService {

	@Autowired
	private SongDao songDao;
	
	
	public Song getSong(Integer songid)
	{
		return songDao.getSongById(songid).get(0);
		
	}
}
