package com.mangly.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for the song
 * @author dev1
 *
 */
@Entity
@Table(name = "t_song", schema="mangly")
public class Song {
	
	
	private Integer id;
	
	private String songName;
	private String singerNames;
	private String lyricistName;
	private String lyrics;
	
	@Id
	@Column(name = "song_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "song_name")
	public String getSongName() {
		return songName;
	}
	public void setSongName(String songName) {
		this.songName = songName;
	}
	
	@Column(name = "song_lyricist_names")
	public String getSingerNames() {
		return singerNames;
	}
	public void setSingerNames(String singerNames) {
		this.singerNames = singerNames;
	}
	
	@Column(name = "singer_names")
	public String getLyricistName() {
		return lyricistName;
	}
	public void setLyricistName(String lyricistName) {
		this.lyricistName = lyricistName;
	}
	
	@Column(name = "lyric")
	public String getLyrics() {
		return lyrics;
	}
	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

}
