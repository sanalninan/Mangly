package com.mangly.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.*; 

import com.zaxxer.hikari.HikariDataSource;


@Configuration
@EnableTransactionManagement
public class DBConfig {
	@Value("${hibernate.driver}")
	private String driver;

	@Value("${hibernate.password}")
	private String password;

	@Value("${hibernate.url}")
	private String url;

	@Value("${hibernate.username}")
	private String user;

	@Value("${hibernate.dialect}")
	private String dialect;

	@Value("${hibernate.packagesToScan}")
	private String folder;
	
	@Bean
	public DataSource buildDataSource() {
	
		HikariDataSource source = new HikariDataSource();

		source.setDriverClassName(driver);
		source.setJdbcUrl(url);
		source.setUsername(user);
		source.setPassword(password);
		return source;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean session = new LocalSessionFactoryBean();
		session.setDataSource(buildDataSource());
		session.setPackagesToScan(folder);
		Properties hibernate = new Properties();
		hibernate.put("hibernate.dialect", dialect);
		hibernate.put("hibernate.show_sql", "false");
		hibernate.put("hibernate.hbm2ddl.auto", "false");
		session.setHibernateProperties(hibernate);
		return session;
	}

	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager m = new HibernateTransactionManager();
		m.setSessionFactory(sessionFactory().getObject());
		return m;
	}

}
