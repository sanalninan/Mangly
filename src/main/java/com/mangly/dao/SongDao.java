package com.mangly.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mangly.entity.Song;

@Service
public class SongDao {
	
	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<Song> getSongById(Integer id){
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Song.class);
		criteria.add(Restrictions.eq("id", id));
		return criteria.list();
	}
}
