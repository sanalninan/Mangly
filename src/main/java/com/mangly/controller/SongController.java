package com.mangly.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mangly.entity.Song;
import com.mangly.service.SongService;

@Controller
@RequestMapping("/song")
public class SongController {
	
	@Autowired
	SongService songService;
	
	@RequestMapping(value="/{songid}", method = RequestMethod.GET)
    public String greeting(@PathVariable("songid") Integer id, Model model) {
        
		Song song = songService.getSong(id);
		
		model.addAttribute("name", song.getSongName());
        return "home";
    }

}
