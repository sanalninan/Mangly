package com.mangly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManglyApplication {

	  public static void main(String[] args) {
	        SpringApplication.run(ManglyApplication.class, args);
	    }


	
}
